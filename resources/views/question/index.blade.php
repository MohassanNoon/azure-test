@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        Questions List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a class="active">Questions List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Questions List</h3>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr</th>
                  <th>Question</th>
                  <th>Quiz Name</th>
                  <th>Option 1</th>
                  <th>Option 2</th>
                  <th>Option 3</th>
                  <th>Option 4</th>
                  <th>Answer</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $count = 1 ?>
                @foreach($data as $val)
                <tr>
                  <td>{{$count}}</td>
                  <td>{{$val->question_text}}</td>
                  <td>{{$val->quiz->name}}</td>
                  <td>{{$val->options->option1}}</td>
                  <td>{{$val->options->option2}}</td>
                  <td>{{$val->options->option3}}</td>
                  <td>{{$val->options->option4}}</td>
                  <td>{{$val->answer}}</td>
                  <td width="10%">
                    <a class="btn btn-success" href="{{url('question/'. $val->id . '/edit/')}}"><i class="fa fa-edit"></i></a>
                    {{-- <button class="btn btn-danger" data-toggle="modal" data-target="#{{$val->id}}"><i class="fa fa-trash"></i></button> --}}
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable()
        })
    </script>
@endsection