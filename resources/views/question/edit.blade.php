@extends('layouts.master')
@section('content')
    <section class="content">

        @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <p>{!! session('flash_message_error') !!}</p>
            </div>

        @endif

        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <p>{!! session('flash_message_success') !!}</p>
            </div>

        @endif

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Edit Question</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('question-update')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Select Quiz</label>
                                            <select class="form-control" name="quiz_id" required>
                                                <option value="">Select Quiz Name</option>
                                                @foreach($quizs as $val)
                                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                    </div>
                                    
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">                                    
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Question</label>
                                            <input type="text" class="form-control" name="question_text" placeholder="Enter Product Name" value={{$question->question_text}}>
                                        </div> 
                                    </div>                                   
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Option1</label>
                                            <input type="text" class="form-control" name="option1" placeholder="Option 1" value={{$question->options->option1}}>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Option2</label>
                                            <input type="text" class="form-control" name="option2" placeholder="Option 2" value={{$question->options->option2}}>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Option3</label>
                                            <input type="text" class="form-control" name="option3" placeholder="Option 3" value={{$question->options->option3}}>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Option4</label>
                                            <input type="text" class="form-control" name="option4" placeholder="Option 4" value={{$question->options->option4}}>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Answer</label>
                                            <input type="text" class="form-control" name="answer" placeholder="answer" value={{$question->answer}}>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection