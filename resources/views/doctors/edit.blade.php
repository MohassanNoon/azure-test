@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Edit Doctor</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('doctor/' . $doctor->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
                                            <input type="text" class="form-control" name="first_name" value="{{$doctor->first_name}}" placeholder="Enter First Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Last Name</label>
                                            <input type="text" class="form-control" name="last_name" value="{{$doctor->last_name}}" placeholder="Enter Last Name">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Doctor Category</label>
                                            <select class="form-control" name="doctor_category_id">
                                                @foreach($doctor_categories as $val)
                                                    @if($val->id == $doctor->doctor_category_id)
                                                        <option selected value="{{$val->id}}">{{$val->name}}</option>
                                                    @else
                                                        <option value="{{$val->id}}">{{$val->name}}</option>
                                                    @endif
                                                    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="text" class="form-control" value="{{$doctor->email}}" name="email" placeholder="Enter Email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone</label>
                                            <input type="text" class="form-control" name="phone" value={{$doctor->phone}} placeholder="Enter Phone Number">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gender</label>
                                            <select class="form-control" name="gender">
                                                <option value="">Select</option>
                                                <option @if($doctor->gender == "Male") selected @endif value="Male">Male</option>
                                                <option @if($doctor->gender == "Female") selected @endif value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Education</label>
                                            <input type="text" class="form-control" value="{{$doctor->education}}" name="education" placeholder="Enter Education">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>DOB:</label>

                                            <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" value="{{$doctor->dob}}" name="dob" id="datepicker">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Image</label>
                                            <input type="file" id="exampleInputFile" name="image">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <img src="{{$doctor->image}}" height="80" width="100">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" rows="2" name="address" placeholder="Enter Address">{{$doctor->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        //Date picker
        $('#datepicker').datepicker({
        autoclose: true
        })
    </script>
@endsection