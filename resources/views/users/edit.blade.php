@extends('layouts.master')
@section('content')
    <section class="content">
        @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <p>{!! session('flash_message_error') !!}</p>
            </div>

        @endif
        
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Edit User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('user/' . $user->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" value="{{$user->name}}" name="name" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" class="form-control" name="email" value="{{$user->email}}" placeholder="Enter Email">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Type</label>
                                            <select class="form-control" name="team_id" required>
                                                <option value="">Select Team</option>
                                                @foreach($teams as $val)
                                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">avatar</label>
                                            <input type="file" id="exampleInputFile" name="avatar">
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <img src="{{$user->avatar}}" width="80" height="100">
                                            </div>
                                        </div>
                                    </div>                         
                                    
                                </div>
                            </div>
                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        //Date picker
        $('#datepicker').datepicker({
        autoclose: true
        })
    </script>
@endsection