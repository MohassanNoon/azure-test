@extends('layouts.master')
@section('content')
    <section class="content">
        @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <p>{!! session('flash_message_error') !!}</p>
            </div>

        @endif
        
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Add User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{route('user.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" class="form-control" name="email" placeholder="Enter Email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Enter Password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Confirm Password</label>
                                            <input type="password" class="form-control" name="confirm_password" placeholder="Enter Confirm Password">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{-- <label for="exampleInputEmail1">Type</label>
                                            <select class="form-control" name="type">
                                                <option value="">Select</option>
                                                <option value="COO">COO</option>
                                                <option value="DMS">DMS</option>
                                                <option value="BUM/BM">BUM/BM</option>
                                                <option value="NSM">NSM</option>
                                                <option value="PM">PM</option>
                                                <option value="SM">SM</option>
                                                <option value="DSM">DSM</option>
                                                <option value="FE">FE</option>
                                                <option value="S0SPO">S0SPO</option>
                                                <option value="SPO">SPO</option>
                                            </select> --}}

                                            <label for="exampleInputEmail1">Select Team</label>
                                            <select class="form-control" name="team_id" required>
                                                <option value="">Select Team</option>
                                                @foreach($teams as $val)
                                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">avatar</label>
                                            <input type="file" id="exampleInputFile" name="avatar">
                                        </div>
                                    </div>                         
                                    
                                </div>
                            </div>
                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        //Date picker
        $('#datepicker').datepicker({
        autoclose: true
        })
    </script>
@endsection