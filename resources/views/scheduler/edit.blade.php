@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Edit Schedule</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('schedule-update')}}">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="col-md-6" style="padding: 0px;">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">User</label>
                                            <select class="form-control" name="user_id" required>
                                                @foreach($users as $val)
                                                    @if($val->id == $data->user_id)
                                                        <option selected value="{{$val->id}}">{{$val->name}}</option>
                                                    @else 
                                                        <option value="{{$val->id}}">{{$val->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">             
                                    
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Doctors</label>
                                        <select class="form-control select2" id="doctors" multiple="multiple" name="doctor_id[]" disabled>
                                            @foreach($doctors as $val)
                                                @if($val->id == $data->doctor_id)
                                                    <option selected value="{{$val->id}}">{{$val->first_name}} {{' ' . $val->last_name}}</option>
                                                @else 
                                                    <option value="{{$val->id}}">{{$val->first_name}} {{' ' . $val->last_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <br>
                                            <label>
                                                <input type="checkbox" name="priority" class="minimal"  value="1">
                                                Priority
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                <label>Meeting Time:</label>

                                                <div class="input-group">
                                                    <input type="text" name="time" class="form-control timepicker">

                                                    <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                                </div>
                                            </div>
                                        </div>                                    
                                </div>
                            </div>

                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            

            $('#doctors').select2();

            //Date range picker
            $('#reservation').daterangepicker({
                dateLimit: { days: 7 },
            })

            //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })

            //Timepicker
            $('.timepicker').timepicker({
            showInputs: false
            })

            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )
        });

        //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
            })


        
    </script>
@endsection