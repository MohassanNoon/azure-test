@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        Schedule List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a class="active">Schedule List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Schedule List</h3>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Doctor</th>
                  <th>Time</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schedules as $val)
                <tr>
                  <td>{{$val->getUser->name}}</td>
                  <td>{{$val->doctor->first_name}} {{' ' . $val->doctor->last_name}}</td>
                  <td>{{$val->time}}</td>
                  <td>{{$val->status}}</td>
                  <td width="10%">
                    <a class="btn btn-success" href="{{url('schedule/'. $val->id . '/edit/')}}"><i class="fa fa-edit"></i></a>
                    {{-- <button class="btn btn-danger" data-toggle="modal" data-target="#{{$val->id}}"><i class="fa fa-trash"></i></button> --}}
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable()
        })
    </script>
@endsection