@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        Result
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Result</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Result</h3>
            </div>            
            
            <div class="box-body">
              <table id="resultt" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Correct</th>
                  <th>Wrong</th>
                  <th>Obtained</th>
                  <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $val)
                <?php $total = $val->correct_answers + $val->wrong_answers;
                ?>
                <tr>
                  <td>{{$val->user->name}}</td>
                  <td>@if($val->correct_answers){{$val->correct_answers}}@else 0 @endif</td>
                  <td>@if($val->wrong_answers){{$val->wrong_answers}}@else 0 @endif</td>
                  <td>{{$total - $val->wrong_answers}}</td>
                  <td>{{$total}}</td>
                  <td width="10%">
                    <a class="btn btn-success" href="{{url('quiz/'. $val->id . '/edit/')}}"><i class="fa fa-edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            $('#resultt').DataTable()
        })
    </script>
@endsection