@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        Quiz
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Quiz</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Quiz</h3>
              <br>
              <br>
              <a href="{{url('quiz/create')}}"><button  type="button" class="btn btn-primary">Add Quiz</button></a>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quizs as $val)
                <tr>
                  <td width="90%">{{$val->name}}</td>
                  <td width="10%">
                    <a class="btn btn-success" href="{{url('quiz/'. $val->id . '/edit/')}}"><i class="fa fa-edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable()
        })
    </script>
@endsection