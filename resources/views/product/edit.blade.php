@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Edit Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('product/' . $product->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Product Name</label>
                                            <input type="text" class="form-control" name="name" value="{{$product->name}}" placeholder="Enter Product Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Product Category</label>
                                            <select class="form-control" name="product_category">
                                                @foreach($product_categories as $val)
                                                    @if($val->id == $product->id)
                                                        <option selected value="{{$val->id}}">{{$val->name}}</option>
                                                    @else
                                                        <option value="{{$val->id}}">{{$val->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dose</label>
                                            <input type="text" class="form-control" name="dose" value="{{$product->dose}}" placeholder="Enter Dose">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Potency Unit</label>
                                            <select class="form-control" name="potency_unit">
                                                <option @if($product->potency_unit == "MG") selected @endif value="MG">MG</option>
                                                <option @if($product->potency_unit == "ML") selected @endif value="ML">ML</option>                                            
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dose Form</label>
                                            <select class="form-control" name="dose_form">
                                                <option @if($product->dose_form == "Tablet") selected @endif value="Tablet">Tablet</option>
                                                <option @if($product->dose_form == "Syp") selected @endif value="Syp">Syp</option>
                                                <option @if($product->dose_form == "Injection") selected @endif value="Injection">Injection</option> 
                                                <option @if($product->dose_form == "Cream") selected @endif value="Cream">Cream</option>                                             
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Price</label>
                                            <input type="text" class="form-control" name="price" value="{{$product->price}}" placeholder="Enter Price">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Image</label>
                                            <input type="file" id="exampleInputFile" name="image">
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <img src="{{$product->image}}" height="80" width="100">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="2" name="description"  placeholder="Enter Description">{{$product->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection