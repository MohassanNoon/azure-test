  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="{{url('dashbord')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('product/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('product')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Doctors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('doctor/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('doctor')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Chemist</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('chemist/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('chemist')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Doctor Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('doctor-category/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('doctor-category')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('product-category/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('product-category')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>



        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('user/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('user')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Scheduler</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('schedule')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('list')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Question</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('question')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('all-questions')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview {{Request::is('quiz') || Request::is('quiz/*') ? 'menu-open' : ''}}">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quiz</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" {{Request::is('quiz') || Request::is('quiz/*') ? 'style="display:block"' : ''}}>
            <li><a href="{{url('quiz/create')}}"><i class="fa fa-circle-o"></i>Create</a></li>
            <li ><a href="{{url('quiz')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

        <li class="treeview {{Request::is('result') || Request::is('result/*') ? 'menu-open' : ''}}">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Result</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" {{Request::is('result') || Request::is('result/*') ? 'style="display:block"' : ''}}>
            <li ><a href="{{url('result')}}"><i class="fa fa-circle-o"></i>List</a></li>
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>