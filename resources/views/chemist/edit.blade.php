@extends('layouts.master')
@section('content')
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Add Chemist</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{url('chemist/' . $chemist->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{$chemist->name}}" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="text" class="form-control" name="email" value="{{$chemist->email}}" placeholder="Enter Email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone</label>
                                            <input type="text" class="form-control" name="phone" value="{{$chemist->phone_no}}" placeholder="Enter Phone Number">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gender</label>
                                            <select class="form-control" name="gender">
                                                <option value="">Select</option>
                                                <option @if($chemist->gender == "Male") selected @endif value="Male">Male</option>
                                                <option @if($chemist->gender == "Female") selected @endif value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>DOB:</label>

                                            <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="dob" value="{{$chemist->dob}}" id="datepicker">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" rows="2" name="address" placeholder="Enter Address">{{$chemist->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Image</label>
                                            <input type="file" id="exampleInputFile" name="image">
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-2">
                                                <img src="{{$chemist->image}}" height="80" width="100">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        //Date picker
        $('#datepicker').datepicker({
        autoclose: true
        })
    </script>
@endsection