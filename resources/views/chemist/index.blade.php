@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        Chemists
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active" ><a href="#">Chemist</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <br>
              <br>
              <a href="{{url('chemist/create')}}"><button  type="button" class="btn btn-primary">Add Chemist</button></a>
            </div>
            <!-- /.box-header -->
            
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone #</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($chemists as $val)
                <tr>
                  <td >{{$val->name}}</td>
                  <td >{{$val->email}}</td>
                  <td >{{$val->phone_no}}</td>
                  <td width="10%">
                    <a class="btn btn-success" href="{{url('chemist/'. $val->id . '/edit/')}}"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#{{$val->id}}"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable()
        })
    </script>
@endsection