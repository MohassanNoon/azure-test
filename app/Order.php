<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table= "order_detail";
    protected $fillable = [
        'user_id',
        'doctor_id',
        'product_id',
        'product_category_id',
        'qty',
        'created_at',
        'updated_at'
    ];
}
