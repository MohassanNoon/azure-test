<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorCategory extends Model
{
    protected $table= "doctor_categories";
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];
}
