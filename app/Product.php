<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table= "products";
    protected $fillable = [
        'product_category_id',
        'name',
        'description',
        'dose_form',
        'potency_unit',
        'dose',
        'price',
        'image',
        'created_at',
        'updated_at'
    ];

    public function product_category() {
        return $this->belongsTo('App\ProductCategory' , 'product_category_id');
    }
}
