<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $table= "meetings";
    protected $fillable = [
        'user_id',
        'doctor_id',	
        'start_time',
        'meeting_status',
        'longitude',
        'latitude',
        'image_url',
        'audio_url',
        'end_time',
        'created_at',
        'updated_at'
    ];
    
}
