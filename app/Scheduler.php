<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{
    protected $table= "user_scheduler_list";
    protected $fillable = [
        'user_id',
        'doctor_id',
        'created_at',
        'updated_at'
    ];

    public function doctor() {
        return $this->belongsTo('App\Doctor' , 'doctor_id');
    }

    public function getUser() {
        return $this->belongsTo('App\User' , 'user_id');
    }
}
