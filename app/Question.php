<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "question";

    public function quiz(){
        return $this->belongsTo(Quiz::class , 'quiz_id');
    }

    public function options(){
        return $this->hasOne(QuestionOption::class);
    }
}
