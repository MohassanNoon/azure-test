<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table= "doctors";
    protected $fillable = [
        'doctor_category_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'gender',
        'dob',
        'education',
        'address',
        'image',
        'created_at',
        'updated_at'
    ];
        
    public function doctor_category() {
        return $this->belongsTo('App\DoctorCategory' , 'doctor_category_id');
    }
}
