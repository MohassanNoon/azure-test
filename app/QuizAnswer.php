<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizAnswer extends Model
{
    protected $table = "quiz_answer";
    protected $fillable = [
        'user_id',
        'quiz_id',
    	'question_id',
    	'answer',
    	'created_at',
    	'updated_at'    	
    ];
}
