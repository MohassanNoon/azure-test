<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizAttemted extends Model
{
    protected $table = "user_test_attempts";
    protected $fillable = [
        'user_id',
        'team_id',
        'quiz_id',
        'correct_answers',
        'wrong_answers',
        'total_marks',
        'obtain_marks',
        'percentage',
    	'created_at',
    	'updated_at'    	
    ];

    public function user() {
        return $this->belongsTo('App\User' , 'user_id');
    }
}
