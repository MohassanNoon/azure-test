<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Doctor;
use App\DoctorCategory;

class DoctorController extends Controller
{
    public function get_doctors() {
        $doctors = Doctor::all();
        return response()->json([
            'data' => $doctors
        ]);
    }

    public function get_all_doctor_categories () {
        $doc_cat = DoctorCategory::all();
        return response()->json([
            'data' => $doc_cat
        ]);
    }
}
