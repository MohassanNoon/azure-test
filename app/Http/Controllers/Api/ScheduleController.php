<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Scheduler;

class ScheduleController extends Controller
{
    public function user_schedule(Request $request) {
        $schedules = Scheduler::where('user_id', $request->user_id)->with(['getUser', 'doctor'])->orderBy('priority', 'DESC')->get();
        return response()->json([
            'data' => $schedules,
        ]);
    }
}
