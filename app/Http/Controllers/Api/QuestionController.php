<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Team;
use App\Question;
use App\QuizAnswer;
use App\QuizAttemted;

class QuestionController extends Controller
{
    public function question_list(Request $request) {
        $quiz = Question::where('team_id', $request->team_id)->first();
        $check = QuizAttemted::where('user_id', $request->id)->where('team_id', $request->team_id)->first();
        if($quiz) {
            if(!$check) {
                $attemp = new QuizAttemted();
                $attemp->user_id = $request->id;
                $attemp->team_id = $request->team_id;
                $attemp->quiz_id = $quiz->quiz_id;
                $attemp->save();
    
                $data = Question::where('team_id', $request->team_id)->with('options', 'quiz')->get();
                if(count($data) > 0) {
                    return response()->json([
                        'status' => true,
                        'message' => 'All Questions',
                        'data' => $data
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'You have already attempted quiz!',
                    'data' => null
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'No Quiz Available!',
                'data' => null
            ]);
        } 
    }

    public function submit_question(Request $request) {
        $question = Question::where('id' , $request->question_id)->first();
        $ans = $question->answer;
        $resp = QuizAttemted::where('user_id', $request->user_id)->where('team_id', $request->team_id)->where('quiz_id', $request->quiz_id)->first();
        if($resp) {
            if($ans == $request->answer) {
                $resp->correct_answers = $resp->correct_answers + 1;
                $resp->save();
            } else {
                $resp->wrong_answers = $resp->wrong_answers + 1;
                $resp->save();
            }
        }
        $ans = new QuizAnswer();
        $ans->user_id = $request->user_id;
        $ans->quiz_id = $request->quiz_id;
        $ans->question_id = $request->question_id;
        $ans->answer = $request->answer;
        $ans->save();

        return response()->json([
            'status' => true,
            'message' => 'Answer Saved',
        ]);
    }
}
