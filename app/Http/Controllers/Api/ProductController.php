<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;

class ProductController extends Controller
{
    public function get_products() {
        $products = Product::with('product_category')->get();
        return response()->json([
            'data' => $products
        ]);
    }

    public function get_category_list () {
        $product_cat = ProductCategory::all();
        return response()->json([
            'data' => $product_cat,
        ]);
    }
}
