<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Meeting;

class MeetingController extends Controller
{
    public function getallmeetings() {
        $meetings = Meeting::all();
        return response()->json([
            'data' => $meetings
        ]);
    }
    public function save(Request $request) {
        $meeting = new Meeting();
        $meeting->user_id = $request->user_id;
        $meeting->doctor_id = $request->doctor_id;
        $meeting->start_time = $request->start_time;
        $meeting->meeting_status = 1;
        $meeting->longitude = $request->longitude;
        $meeting->latitude = $request->latitude;
        $meeting->save();

        return response()->json([
            'message' => 'Meeting Start Successfully',
            'd1at' => $meeting,
            ]);
    }
}
