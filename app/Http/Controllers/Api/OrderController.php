<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function get_all_orders() {
        $order = Order::all();
        return response()->json([
            'data' => $order
        ]);
    }
    public function order_place(Request $request) {
        $order_place = new Order();
        $order_place->user_id = $request->user_id;
        $order_place->doctor_id = $request->doctor_id;
        $order_place->product_id = $request->product_id;
        $order_place->product_category_id = $request->product_category_id;
        $order_place->qty = $request->qty;
        $order_place->save();

        return response()->json([
            'message' => 'Order Place Sussessfully',
            'data' => $order_place
        ]);
    }
}
