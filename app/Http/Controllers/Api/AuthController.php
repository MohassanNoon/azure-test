<?php

namespace App\Http\Controllers\Api;

// use App\Mail\ForgetPasswordRequestMail;
// use App\Mail\ResetPasswordMail;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request){
        $user = User::where('email' , $request->email)->first();

        if ($user){
            if ($user->email == $request->email){
                if (Hash::check($request->password , $user->password)){
                    $token = Str::uuid();
                    $user = User::find($user->id);
                    $user->remember_token = $token;
                    $user->save();

                    return response()->json([
                       'token' => $token,
                       'data' => $user,
                       'message' => "Successfully login"
                    ]);
                }
                else{
                    return response()->json([
                        "status" => false,
                        "token" => null,
                        "message" => 'Password Mismatch'
                    ]);
                }
            }
        }
        else{
            return response()->json([
                "token" => null,
                "message" => 'Email Not Found'
            ]);
        }
    }

    public function password_change(Request $request) {
        $change = User::where('team_id', $request->team_id)->get();
        foreach($change as $val) {
            User::where('id', $val->id)->update([
                'password' => Hash::make($request->password)
            ]);
        }

        return response()->json([
            "status" => true,
            "message" => 'password change'
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
                'status' => true,
                'message' => 'Successfully logged out',
                'data' => null
        ]);
    }
}
