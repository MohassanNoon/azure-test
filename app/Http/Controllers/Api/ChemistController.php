<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Chemist;

class ChemistController extends Controller
{
    public function get_chemist() {
        $chemists = Chemist::all();
        return response()->json([
            'data' => $chemists
        ]);
    }
}
