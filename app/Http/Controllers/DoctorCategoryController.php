<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DoctorCategory;

class DoctorCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doc_category = DoctorCategory::all();
        return view('doctor_category.index', compact('doc_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('doctor_category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $doc_cat = DoctorCategory::where('name' , $request->name)->first();
        if($doc_cat) {
            return redirect()->back()->with('flash_message_error','Doctor Category Name Already Exist');
        } else {
            $doc_cat = new DoctorCategory;
            $doc_cat->name = $request->name;
            $doc_cat->save();

            return redirect(url('doctor-category'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DoctorCategory::where('id', $id)->first();
        return view('doctor_category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DoctorCategory::where('id' , $id)->update([
            'name' => $request->name,
        ]);

        return redirect(url('doctor-category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
