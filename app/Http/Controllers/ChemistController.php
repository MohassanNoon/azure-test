<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chemist;

class ChemistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chemists = Chemist::all();
        return view('chemist.index', compact('chemists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chemist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $chemist = new Chemist();
        $chemist->name = $request->name;
        $chemist->email = $request->email;
        $chemist->phone_no = $request->phone;
        $chemist->gender = $request->gender;
        $chemist->dob = $request->dob;
        $chemist->address = $request->address;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/chemists';
            $file->move($destination_path, $image);
            $chemist->image = url('/images/chemists' . '/' . $image);
        }
        $chemist->save();

        return redirect(url('chemist'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chemist = Chemist::where('id', $id)->first();
        return view('chemist.edit', compact('chemist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Chemist::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone_no' => $request->phone,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'address' => $request->address
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/chemists';
            $file->move($destination_path, $image);
            Chemist::where('id',$id)->update([
                'image' => url('/images/chemists' . '/' . $image)
            ]);
        }

        return redirect(url('chemist'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
