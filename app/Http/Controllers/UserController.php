<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Mail\ResetPasswordEmail;
use App\User;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Team;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::get();
        return view('users.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $user = User::where('email' , $request->email)->first();

        if ($user){
            if ($user->email == $request->email){
                return redirect()->back()->with('flash_message_error','Email Already exist');
            }
        }
        else{
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->type = $request->type;
            $user->team_id = $request->team_id;

            if ($request->hasFile('avatar')) {
                $file = $request->avatar;
                $filename = $file->getClientOriginalName();
                $image = date('His') . $filename;
                $destination_path = public_path() . '/images/users';
                $file->move($destination_path, $image);
                $user->avatar = url('/images/users' . '/' . $image);
            }

            $user->save();

            return redirect(url('user'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = Team::get();
        $user = User::where('id', $id)->first();
        return view('users.edit', compact('user' , 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id' , $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'team_id' => $request->team_id,
        ]);

        if ($request->hasFile('avatar')) {
            $file = $request->avatar;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/users';
            $file->move($destination_path, $image);
            User::where('id',$id)->update([
                'image' => url('/images/users' . '/' . $image)
            ]);
        }

        return redirect(url('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
