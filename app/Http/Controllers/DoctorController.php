<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\DoctorCategory;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all();
        return view('doctors.index', compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctor_categories = DoctorCategory::all();
        return view('doctors.create', compact('doctor_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $doctor = new Doctor();
        $doctor->first_name = $request->first_name;
        $doctor->last_name = $request->last_name;
        $doctor->doctor_category_id = $request->doctor_category_id;
        $doctor->email = $request->email;
        $doctor->phone = $request->phone;
        $doctor->gender = $request->gender;
        $doctor->dob = $request->dob;
        $doctor->education = $request->education;
        $doctor->address = $request->address;
        
        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/doctors';
            $file->move($destination_path, $image);
            $doctor->image = url('/images/doctors' . '/' . $image);
        }

        $doctor->save();
        return redirect(url('doctor'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor = Doctor::where('id' , $id)->first();
        $doctor_categories = DoctorCategory::all();
        return view('doctors.edit' , compact('doctor', 'doctor_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $data = Doctor::where('id' , $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'doctor_category_id' => $request->doctor_category_id,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'education' => $request->education,
            'address' => $request->address
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/doctors';
            $file->move($destination_path, $image);
            Doctor::where('id',$id)->update([
                'image' => url('/images/doctors' . '/' . $image)
            ]);
        }

        return redirect(url('doctor'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
