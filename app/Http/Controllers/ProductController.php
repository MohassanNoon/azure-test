<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use Dotenv\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = ProductCategory::all();
        return view('product.create', compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->hasfile('image'));
        $request->validate([
            'name' => 'required'
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->product_category_id = $request->product_category;
        $product->dose = $request->dose;
        $product->potency_unit = $request->potency_unit;
        $product->dose_form = $request->dose_form;
        $product->price = $request->price;
        $product->description = $request->description;
        
        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/products';
            $file->move($destination_path, $image);
            $product->image = url('/images/products' . '/' . $image);
        }
        
        $product->save();

        return redirect(url('product'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories = ProductCategory::all();
        $product = Product::where('id' , $id)->first();
        return view('product.edit', compact('product' , 'product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        Product::where('id', $id)->update([
            'product_category_id' => $request->product_category,
            'name' => $request->name,
            'dose' => $request->dose,
            'potency_unit' => $request->potency_unit,
            'dose_form' => $request->dose_form,
            'price' => $request->price,
            'description' => $request->description
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $image = date('His') . $filename;
            $destination_path = public_path() . '/images/products';
            $file->move($destination_path, $image);
            Product::where('id',$id)->update([
                'image' => url('/images/products' . '/' . $image)
            ]);
        }

        return redirect(url('product'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
