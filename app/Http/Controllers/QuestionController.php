<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Question;
use App\QuestionOption;
use App\Team;

class QuestionController extends Controller
{
    public function index() {
        $data = Question::with('quiz', 'options')->get();
        return view('question.index', compact('data'));
    }

    public function edit($id) {
        $quizs = Quiz::all();
        $question = Question::where('id', $id)->with('quiz', 'options')->first();
        return view('question.edit', compact('question', 'quizs'));
    }

    public function update_question(Request $request) {
        Question::where('id', $request->id)->update([
            'quiz_id' => $request->quiz_id,
            'question_text' => $request->question_text,
            'answer' => $request->answer
        ]);
        QuestionOption::where('question_id', $request->id)->update([
            'option1' => $request->option1,
            'option2' => $request->option2,
            'option3' => $request->option3,
            'option4' => $request->option4
        ]);

        return redirect(url('all-questions'));
    }

    public function show_question_form() {
        $quizs = Quiz::get();
        $teams = Team::get();
        return view('question.create', compact('quizs', 'teams'));
    }

    public function save(Request $request) {
        // dd($request->all());
        if($request->quiz_id) {
            $question = new Question;
            $question->quiz_id = $request->quiz_id;
            $question->question_text = $request->question_text;
            $question->answer = $request->answer;
            $question->save();

            $question_option = new QuestionOption;
            $question_option->question_id = $question->id;
            $question_option->option1 = $request->option1;
            $question_option->option2 = $request->option2;
            $question_option->option3 = $request->option3;
            $question_option->option4 = $request->option4;
            $question_option->save();

            return redirect(url('question'))->with('flash_message_success','Question Add Successfully');
        } else {
            return redirect()->back()->with('flash_message_error','Select Quiz Name');
        }
    }
}
