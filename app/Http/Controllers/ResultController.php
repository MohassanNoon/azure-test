<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuizAttemted;

class ResultController extends Controller
{
    public function get_result(Request $request) {
        $results = QuizAttemted::with('user')->get();
        return view('result.result', compact('results'));
    }
}
