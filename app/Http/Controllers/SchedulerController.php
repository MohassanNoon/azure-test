<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Doctor;
use App\Scheduler;

class SchedulerController extends Controller
{
    public function show_scheduler_list() {
        $users = User::all();
        $doctors = Doctor::all();
        return view('scheduler.index', compact('users' , 'doctors'));
    }

    public function save (Request $request) {
        
        
        if($request->user_id) {
            if($request->doctor_id) {
                foreach($request->doctor_id as $val) {                    
                    $data = new Scheduler();
                    $data->user_id = $request->user_id;
                    $data->doctor_id = $val;
                    if($request->date) {
                        $date = explode(" - ",$request->date);
                        $data->start_date = $date[0];
                        $data->end_date = $date[1];
                    }
                    $data->status = "pending";
    
                    $data->save();                    
                }
                return redirect(url('list'));
            }            
        }
    }

    public function get_all_schedule () {
        $schedules = Scheduler::with(['getUser', 'doctor'])->orderBy('priority', 'DESC')->get();
        return view('scheduler.list' , compact('schedules'));
    }

    public function show_edit($id) {
        $users = User::all();
        $doctors = Doctor::all();
        $data = Scheduler::where('id' , $id)->with(['getUser', 'doctor'])->first();
        // dd($data);
        return view('scheduler.edit' , compact('data', 'users', 'doctors'));
    }

    public function update_schedular(Request $request) {
        // dd($request->all());
        $data = Scheduler::where('user_id', $request->user_id)->first();
        $data->time = $request->time;
        if($request->priority) {
            $data->priority = $request->priority;
        }
        $data->save();
        return redirect(url('list'));
    }
}
