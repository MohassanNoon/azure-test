<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::prefix('admin')->group(function () {
    Auth::routes();
    Route::group(['middleware' => 'auth'], function (){
        Route::get('/' , 'DashbordController@dashbord');
        Route::resource('product-category','ProductCategoryController');
    });
});



// Route::group(['prefix' => 'product-category'], function () {
//     Route::get('' , 'ProductCategoryController@index');
// });


// product
Route::resource('product' , 'ProductController');
//Doctor Category
Route::resource('doctor-category', 'DoctorCategoryController');
//Doctors
Route::resource('doctor', 'DoctorController');
// Chemist
Route::resource('chemist', 'ChemistController');
// User
Route::resource('user', 'UserController');
// Quiz
Route::resource('quiz', 'QuizController');

Route::get('schedule' , 'SchedulerController@show_scheduler_list');
Route::post('schedule' , 'SchedulerController@save');
Route::get('list' , 'SchedulerController@get_all_schedule');
Route::get('schedule/{id}/edit' , 'SchedulerController@show_edit');
Route::post('schedule-update' , 'SchedulerController@update_schedular');

Route::get('question' , 'QuestionController@show_question_form');
Route::get('all-questions' , 'QuestionController@index');
Route::get('question/{id}/edit' , 'QuestionController@edit');
Route::post('question-update' , 'QuestionController@update_question');
Route::post('question' , 'QuestionController@save');
Auth::routes();

Route::get('/result', 'ResultController@get_result');
Route::get('/home', 'HomeController@index')->name('home');
