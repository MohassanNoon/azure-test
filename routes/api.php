<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** AUTHENTICATION ROUTES */
Route::post('login', 'Api\AuthController@login');
Route::get('logout', 'Api\AuthController@logout');

// Doctor List
Route::get('get-doctors' , 'Api\DoctorController@get_doctors');
// Chemist List
Route::get('get-chemists' , 'Api\ChemistController@get_chemist');
// Product List
Route::get('get-products' , 'Api\ProductController@get_products');
// get all doctor categories
Route::get('doctor-categories' , 'Api\DoctorController@get_all_doctor_categories');
//product category get all
Route::get('prodduct-categories' , 'Api\ProductController@get_category_list');
// user Schedule get using user_id
Route::post('schedule' , 'Api\ScheduleController@user_schedule');

// Meeting Start with Doctor
Route::post('meeting' , 'Api\MeetingController@save');
Route::get('all-meetings', 'Api\MeetingController@getallmeetings');
// Order placed
Route::post('order-place', 'Api\OrderController@order_place');
Route::get('all-orders', 'Api\OrderController@get_all_orders');

Route::get('question-list', 'Api\QuestionController@question_list');
Route::post('submit-question', 'Api\QuestionController@submit_question');


Route::post('password-change', 'Api\AuthController@password_change');

